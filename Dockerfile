FROM tensorflow/tensorflow:latest-jupyter

RUN pip install --upgrade pip

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

RUN apt-get install graphviz -y

RUN dot -c